import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

import CustomerOrder from './customer-order';
import CostCalculator from './cost-calculator';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
  public orders: CustomerOrder[];
  public calculator: CostCalculator;

  public orderForm: FormGroup;

  private orderNumber = 1;

  get appleCount() { return this.orderForm.get('appleCount'); }
  get bananaCount() { return this.orderForm.get('bananaCount'); }
  get carrotCount() { return this.orderForm.get('carrotCount'); }

  constructor(private formBuilder: FormBuilder) {
    this.orderForm = this.formBuilder.group({
      appleCount: [0, [Validators.required, Validators.min(0), this.partialOrderValidator()]],
      bananaCount: [0, [Validators.required, Validators.min(0), this.partialOrderValidator()]],
      carrotCount: [0, [Validators.required, Validators.min(0), this.partialOrderValidator()]],
    });
   }

  public isAuthorized = false;

  ngOnInit() {
    this.orders = [];
    this.calculator = new CostCalculator(this.orders);
  }

  addOrder() {
    const apples = this.parseOrderCount(this.orderForm.value.appleCount);
    const bananas = this.parseOrderCount(this.orderForm.value.bananaCount);
    const carrots = this.parseOrderCount(this.orderForm.value.carrotCount);

    if (!apples && !bananas && !carrots) {
      return; // invalid order
    }

    const newOrder = new CustomerOrder(this.orderNumber++, apples, bananas, carrots);
    this.orders.push(newOrder);
    this.calculator = new CostCalculator(this.orders);

    this.orderForm.reset({
      appleCount: 0,
      bananaCount: 0,
      carrotCount: 0
    });
  }

  private getWholeNumber(val) {
    if (!val || !val.toFixed) {
      return 0;
    }
    return Number(val.toFixed(0));
  }

  private parseOrderCount(val) {
    return Math.max(0, this.getWholeNumber(val));
  }

  partialOrderValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const forbidden = Number(control.value) !== this.getWholeNumber(control.value);
      return forbidden ? {'partial': {value: control.value}} : null;
    };
  }
}
