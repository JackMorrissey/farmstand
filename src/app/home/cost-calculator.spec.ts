import CostCalculator from './cost-calculator';
import CustomerOrder from './customer-order';

describe('Cost Calculator', () => {
  describe('Empty Order', () => {
    let calculator: CostCalculator;
    beforeEach(() => {
      calculator = new CostCalculator([
        new CustomerOrder(1, 0, 0, 0)
      ]);
    });

    it('should cost a total price of 0', () => {
      expect(calculator.totalCost).toBe(0);
    });

    it('should order 0 products', () => {
      expect(calculator.orderedAppleCount).toBe(0);
      expect(calculator.orderedBananaCount).toBe(0);
      expect(calculator.orderedCarrotCount).toBe(0);
      expect(calculator.extraCarrotsPurchasedCount).toBe(0);
      expect(calculator.requiredCarrotPurchaseCount).toBe(0);
    });
  });

  describe('Single Simple Order', () => {
    let calculator: CostCalculator;
    beforeEach(() => {
      calculator = new CostCalculator([
        new CustomerOrder(1, 1, 1, 3)
      ]);
    });

    it('should have the correct total price', () => {
      expect(calculator.totalCost).toBe(6.50);
    });

    it('should order the correct number of product', () => {
      expect(calculator.orderedAppleCount).toBe(1);
      expect(calculator.orderedBananaCount).toBe(1);
      expect(calculator.orderedCarrotCount).toBe(3);
      expect(calculator.extraCarrotsPurchasedCount).toBe(0);
      expect(calculator.requiredCarrotPurchaseCount).toBe(3);
    });
  });

  describe('Single Order With Carrot Shortage', () => {
    let calculator: CostCalculator;
    beforeEach(() => {
      calculator = new CostCalculator([
        new CustomerOrder(1, 0, 0, 10)
      ]);
    });

    it('should have the correct total price', () => {
      expect(calculator.totalCost).toBe(12);
    });

    it('should order the correct number of products', () => {
      expect(calculator.orderedAppleCount).toBe(0);
      expect(calculator.orderedBananaCount).toBe(0);
      expect(calculator.orderedCarrotCount).toBe(10);
      expect(calculator.extraCarrotsPurchasedCount).toBe(2);
      expect(calculator.requiredCarrotPurchaseCount).toBe(12);
    });
  });

  describe('Multiple Simple Orders', () => {
    let calculator: CostCalculator;
    beforeEach(() => {
      calculator = new CostCalculator([
        new CustomerOrder(1, 1, 1, 3),
        new CustomerOrder(2, 2, 5, 9),
        new CustomerOrder(3, 6, 9, 6),
        new CustomerOrder(4, 1, 4, 0),
        new CustomerOrder(5, 7, 3, 12)
      ]);
    });

    it('should have the correct total price', () => {
      expect(calculator.totalCost).toBe(97);
    });

    it('should order the correct number of product', () => {
      expect(calculator.orderedAppleCount).toBe(17);
      expect(calculator.orderedBananaCount).toBe(22);
      expect(calculator.orderedCarrotCount).toBe(30);
      expect(calculator.extraCarrotsPurchasedCount).toBe(0);
      expect(calculator.requiredCarrotPurchaseCount).toBe(30);
    });
  });

  describe('Multiple Non-Bundle Carrot Orders', () => {
    let calculator: CostCalculator;
    beforeEach(() => {
      calculator = new CostCalculator([
        new CustomerOrder(1, 1, 0, 2),
        new CustomerOrder(2, 0, 1, 2),
        new CustomerOrder(3, 2, 0, 4),
        new CustomerOrder(4, 0, 1, 4),
        new CustomerOrder(5, 5, 5, 5),

      ]);
    });

    it('should have the correct total price', () => {
      expect(calculator.totalCost).toBe(44.5);
    });

    it('should order the correct number of product', () => {
      expect(calculator.orderedAppleCount).toBe(8);
      expect(calculator.orderedBananaCount).toBe(7);
      expect(calculator.orderedCarrotCount).toBe(17);
      expect(calculator.extraCarrotsPurchasedCount).toBe(1);
      expect(calculator.requiredCarrotPurchaseCount).toBe(18);
    });
  });

});
