import CustomerOrder from './customer-order';

const appleCost = 2.00;
const bananaCost = 1.50;
const carrotCost = 1.00;

export default class OrderCalculator {
  orderedAppleCount = 0;
  orderedBananaCount = 0;
  orderedCarrotCount = 0;
  requiredCarrotPurchaseCount = 0;
  extraCarrotsPurchasedCount = 0;
  totalCost = 0;

  // export for convenience
  appleCost = appleCost;
  bananaCost = bananaCost;
  carrotCost = carrotCost;

  constructor(orders: CustomerOrder[]) {
    orders.forEach((order) => {
      this.orderedAppleCount += order.appleCount;
      this.orderedBananaCount += order.bananaCount;
      this.orderedCarrotCount += order.carrotCount;
    });

    const carrotCountOverBulkRequirement = this.orderedCarrotCount % 3;
    if (carrotCountOverBulkRequirement > 0) {
      this.extraCarrotsPurchasedCount = 3 - carrotCountOverBulkRequirement;
    }

    this.requiredCarrotPurchaseCount = this.orderedCarrotCount + this.extraCarrotsPurchasedCount;

    this.totalCost += this.orderedAppleCount * appleCost;
    this.totalCost += this.orderedBananaCount * bananaCost;
    this.totalCost += this.requiredCarrotPurchaseCount * carrotCost;
  }
}
