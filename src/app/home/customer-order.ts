export default class CustomerOrder {
  constructor(public orderNumber = 1, public appleCount = 0, public bananaCount = 0, public carrotCount = 0 ) {}
}
